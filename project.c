#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include"project.h"
void initNumber(Number *ptr){
	(*ptr) = (Number ) malloc(sizeof(def));
	(*ptr)->sign = '+';
	(*ptr)->pos = 0;
	(*ptr)->tail = (*ptr)->head = NULL;
}

int isDigit(char ch){
	return(ch >= '0' && ch <= '9');
}
void storenum(Number *a1, char *str) {
	initNumber(a1);
	//printf("In storenum passed string: %s\n", str);
	if(str[0] == '-' || str[0] == '+') {
                (*a1)->sign = str[0];
		str++;
        }
	//printf("In storenum() str After assigning sign:%s\n", str);
	char *savestr1, *tok;
	int len, num = 0, length1, i, initial = 0;
	list *temp;
	tok = strtok_r(str, ".", &savestr1);
	len = strlen(tok);
	//printf("In storenum() tok:%s", tok);
	(*a1)->pos = len;
	for(i = 0; i < len ; ){
		if(i != (len - 1) && initial == 0 && tok[i] == '0'){
			i++;
			(*a1)->pos -= 1;
			continue;
		}
		if(isDigit(tok[i])){
	 		num = tok[i] - '0';
	 		initial = 1;
	 		i++;
	 	}
	 	else{
	 		(*a1) = NULL;
			return;
	 	}
	 	temp = (list *)malloc(1 * sizeof(list));
	 	temp->num = num;
	 	temp->next = NULL;
	 	if((*a1)->head == NULL){
			(*a1)->head = (*a1)->tail = temp;
			temp->pre = NULL;
		}
		else{
			temp->num = num;
			temp->pre = (*a1)->tail;
			(*a1)->tail->next = temp;
			(*a1)->tail = temp;
		}
	}
	length1 = strlen(savestr1);
	for(i = 0; i < length1;){
		if(isDigit(savestr1[i])){
			num = savestr1[i] - '0';
				i++;
		}
		else{
			(*a1) = NULL;
			return;
		}	
		temp = (list *)malloc(1 * sizeof(list));
	 	temp->num = num;
	 	temp->next = NULL;
		if((*a1)->head == NULL){
			(*a1)->head = (*a1)->tail = temp;
			temp->pre = NULL;
		}
		else{
			temp->num = num;
			temp->pre = (*a1)->tail;
			(*a1)->tail->next = temp;
			(*a1)->tail = temp;
		}
	}
	processNumber(a1);
//	printNumber(*a1);	
}
void printNumber(Number a){
	list *temp = a->head;
	int i;
	if(a->sign != '+')
		printf("%c", a->sign);
	for(i = 0; temp != NULL; i++){
		if(i == a->pos){
			printf("%c", '.');
			printf("%d",temp->num);
		}
		else
			printf("%d", temp->num);
		temp = temp->next;
	}
	printf("\n");
}
int NodesInNumber(Number number) {
	int count = 0;
	list *temp = number->head;
	while(temp != NULL) {
		count += 1;
		temp = temp->next;
	}
	return (count);
}
Number add(Number num1, Number num2) {
	int i, absdiff, num = 0, carry = 0;
	Number num3;
	list *temp, *temptail1, *temptail2;
	initNumber(&num3);
	int lenFracNum1 = NodesInNumber(num1) - num1->pos;
	int lenFracNum2 = NodesInNumber(num2) - num2->pos;
	absdiff = abs(lenFracNum1 - lenFracNum2);
	if(num1->pos > num2->pos)
		num3->pos = num1->pos;
	else
		num3->pos = num2->pos;
	if(lenFracNum1 > lenFracNum2){
		temptail1 = num1->tail;
		temptail2 = num2->tail;
	}
	else {
		temptail1 = num2->tail;
		temptail2 = num1->tail;
	}
	for(i = 0; i < absdiff; i++) {
		temp = (list *)malloc(sizeof(list));
		temp->num = temptail1->num;
		//printf("diff:%d\n", temp->num);
		temp->pre = NULL;
		if(num3->head == NULL) {
			temp->pre = temp->next = NULL;
			num3->head = num3->tail = temp;
		}
		else{
			temp->next = num3->head;
			num3->head->pre = temp;
			num3->head = temp;
		}
		temptail1 = temptail1->pre;
	}
	while(temptail1 != NULL || temptail2 != NULL){
		if(temptail1 != NULL && temptail2 != NULL){
			num = (temptail1->num) + (temptail2->num) + carry;
		//	printf("%d %d\n", num, temptail2->num);
			carry = num / 10;
			num = num % 10;
			temptail1 = temptail1->pre;
			temptail2 = temptail2->pre;
		}
		else if(temptail1 != NULL){
			num = (temptail1->num) + carry;
			carry = num / 10;
			num = num % 10;
			temptail1 = temptail1->pre;
		}
		else{
			num = (temptail2->num) + carry;
			carry = num /10;
			num = num % 10;
			temptail2 = temptail2->pre;
		}
		temp = (list *)malloc(sizeof(list));
		temp->num = num;
		temp->pre = NULL;
		if(num3->head == NULL) {
			temp->pre = temp->next = NULL;
			num3->head = num3->tail = temp;
		}
		else{
			temp->next = num3->head;
			num3->head->pre = temp;
			num3->head = temp;
		}
		
	}
	if(carry){
		temp = (list *)malloc(1 * sizeof(list));
		temp->num = carry;
		temp->pre = NULL;
		temp->next = num3->head;
		num3->head->pre = temp;
		num3->head = temp;
		num3->pos += 1;
	}
	return num3;
	
}
/*	Take two number as argument 
*	make absulute camparison
*	if numbers are -3 & +4 then it will campare 3 & 4
*	return -1
*	if 1st num is greater than 2nd number then it will return 1
*	if 1st == 2nd then return 0
*	if 1st > 2nd return 
*/
int abscmp(Number num1, Number num2) {	
	if(num1->pos > num2->pos) {
		return 1;
	}
	if(num1->pos < num2->pos) {
		return -1;
	}
	list *temp1, *temp2;
	temp1 = num1->head;
	temp2 = num2->head;
	while(temp1 != NULL && temp2 != NULL) {
		if(temp1->num > temp2->num) {
			return 1;
		}
		if(temp1->num < temp2->num) {
			return -1;
		}
		temp1 = temp1->next;
		temp2 = temp2->next;
	}
	while(temp1 != NULL){
		if(temp1->num > 0) {
			return 1;
		}
		temp1 = temp1->next;
	}
	while(temp2 != NULL){
		if(temp2->num > 0) {
			return -1;
		}
		temp2 = temp2->next;
	}
	return 0;
}
/* these function remove 0 from initial of Number */
void processNumber(Number *num){
	list *temp = (*num)->head;
	int i = 0, flag = (*num)->pos;	
	while(temp->num == 0) {
		if(i < flag - 1){
			(*num)->head = (*num)->head->next;
			(*num)->head->pre = NULL;
			free(temp);
			(*num)->pos -= 1;
			temp = (*num)->head;
			i++;
		}
		else
			break;
	}
	temp = (*num)->tail;
	i = 0;
	flag = NodesInNumber(*num) - (*num)->pos;
	while(temp->num == 0 && i < flag) {
		(*num)->tail = (*num)->tail->pre;
		(*num)->tail->next = NULL;
		free(temp);
		temp = (*num)->tail;
		i++;
	}
}
Number subtract(Number num1, Number num2) {
	int i, absdiff, num = 0, carry = 0;
	Number num3;
	list *temp, *temptail1, *temptail2;
	initNumber(&num3);
	int lenFracNum1 = NodesInNumber(num1) - num1->pos;
	int lenFracNum2 = NodesInNumber(num2) - num2->pos;
	absdiff = abs(lenFracNum1 - lenFracNum2);
	num3->pos = num1->pos;
	//else
	//	num3->pos = num2->pos;
	temptail1 = num1->tail;
	temptail2 = num2->tail;
	if(lenFracNum1 > lenFracNum2){
		for(i = 0; i < absdiff; i++) {
			temp = (list *)malloc(sizeof(list));
			temp->num = temptail1->num;
			//printf("diff:%d\n", temp->num);
			temp->pre = NULL;
			if(num3->head == NULL) {
				temp->pre = temp->next = NULL;
				num3->head = num3->tail = temp;
			}
			else{
				temp->next = num3->head;
				num3->head->pre = temp;
				num3->head = temp;
			}
			temptail1 = temptail1->pre;
		}
	}
	else {
		for(i = 0; i < absdiff; i++) {
			temp = (list *)malloc(sizeof(list));
			if((temptail2->num + carry) > 0) {
				temp->num = 10 - (temptail2->num + carry);
				carry = 1;
			}
			else{
				temp->num = 0;
				carry = 0;
			}
			//printf("diff:%d\n", temp->num);
			temp->pre = NULL;
			if(num3->head == NULL) {
				temp->pre = temp->next = NULL;
				num3->head = num3->tail = temp;
			}
			else{
				temp->next = num3->head;
				num3->head->pre = temp;
				num3->head = temp;
			}
			temptail2 = temptail2->pre;
		}
	}
	while(temptail1 != NULL || temptail2 != NULL){
		if(temptail1 != NULL && temptail2 != NULL){
			if((temptail2->num + carry) > (temptail1->num)){
				num = (temptail1->num + 10) - (temptail2->num + carry);
				carry = 1;
			}
			else{
				num = (temptail1->num) - (temptail2->num + carry);
				carry = 0;
			}
		//	printf("%d %d\n", num, temptail2->num);
			temptail1 = temptail1->pre;
			temptail2 = temptail2->pre;
		}
		else if(temptail1 != NULL){
			if(carry > (temptail1->num)){
				num = (temptail1->num + 10) - carry;
				carry = 1;
			}
			else {
				num = temptail1->num - carry;
				carry = 0;
			}
			temptail1 = temptail1->pre;
		}
		temp = (list *)malloc(sizeof(list));
		temp->num = num;
		temp->pre = NULL;
		if(num3->head == NULL) {
			temp->pre = temp->next = NULL;
			num3->head = num3->tail = temp;
		}
		else{
			temp->next = num3->head;
			num3->head->pre = temp;
			num3->head = temp;
		}	
	}
//**	printNumber(num3);
	processNumber(&num3);
//**	printNumber(num3);
	return num3;
}
Number addnumbers(Number num1, Number num2){
	Number num3;
	char str[] = {'0', '\0'};
	int cmp;
	if(num1->sign == num2->sign){
		num3 = add(num1, num2);
		num3->sign = num1->sign;
		return num3;
	}
	else{
		cmp = abscmp(num1, num2);
		//**printf("From addnumbers cmp:%d\n", cmp);
		//**printNumber(num1);
		//**printNumber(num2);
		if(cmp == 0){
			storenum(&num3, str);
			printNumber(num3);
			return num3;
		}
		if(cmp == 1){
			num3 = subtract(num1, num2);
			num3->sign = num1->sign;
			return num3;
		}
		else{
			num3 = subtract(num2, num1);
			num3->sign = num2->sign;
			return num3;
		}
	}
}
Number subtractnumbers(Number num1, Number num2){
	Number num3;
	char str[] = {'0', '\0'};
	int cmp;
	if(num1->sign != num2->sign){
		num3 = add(num1, num2);
		num3->sign = num1->sign;
		return num3;
	}
	else{
		cmp = abscmp(num1, num2);
		//**printf("From addnumbers cmp:%d\n", cmp);
		//**printNumber(num1);
		//**printNumber(num2);
		if(cmp == 0){
			storenum(&num3, str);
			printNumber(num3);
			return num3;
		}
		if(cmp == 1){
			num3 = subtract(num1, num2);
			num3->sign = num1->sign;
			return num3;
		}
		else{
			num3 = subtract(num2, num1);
			if(num2->sign == '-')
				num3->sign = '+';
			else
				num3->sign = '-';
			return num3;
		}
	}
}
Number multipliaction(Number num1, Number num2) {
	list *temp1, *temp2 = num2->tail, *temp;
	Number num3, result;
	int lenFracNum1 = NodesInNumber(num1) - num1->pos;
	int lenFracNum2 = NodesInNumber(num2) - num2->pos;
	int i, k = 0, carry;
	while(temp2 != NULL) {
		temp1 = num1->tail;
		carry = 0;
		initNumber(&num3);
		// Added i k number of ZERO's at end of number 
		for(i = 0; i < k; i++){
			temp = (list *)malloc(sizeof(list));
			temp->num = 0;
			temp->pre = NULL;
			if(num3->head == NULL) {
				temp->pre = temp->next = NULL;
				num3->head = num3->tail = temp;
			}
			else{
				temp->next = num3->head;
				num3->head->pre = temp;
				num3->head = temp;
			}
			(num3->pos)++; 
		}
		while(temp1 != NULL) {
			temp = (list *)malloc(sizeof(list));
			temp->num = ((temp1->num * temp2->num) + carry) % 10;
			carry = ((temp1->num * temp2->num) + carry) / 10;
			temp->pre = NULL;
			if(num3->head == NULL) {
				temp->pre = temp->next = NULL;
				num3->head = num3->tail = temp;
			}
			else{
				temp->next = num3->head;
				num3->head->pre = temp;
				num3->head = temp;
			}
			(num3->pos)++;
			temp1 = temp1->pre;
		}
		if(carry){
			temp = (list *)malloc(1 * sizeof(list));
			temp->num = carry;
			temp->pre = NULL;
			temp->next = num3->head;
			num3->head->pre = temp;
			num3->head = temp;
			num3->pos++;
		}
		if(k != 0) {
			result = add(result, num3);
		}
		else {
			result = num3;
		}
		temp2 = temp2->pre;
		k++;
			
	}
	result->pos = result->pos - (lenFracNum1 + lenFracNum2);
	if(num1->sign != num2->sign)
		result->sign = '-';
	processNumber(&result);
	return result;
}
Number division(Number num1,Number num2){
	Number result = NULL, tmpNum1;
	char str[] = "0";
	storenum(&result,str);
	if(abscmp(num2, result) == 0) {
		printf("Invalid operation:(divided by 0)\n");
		return NULL;
	}
	initNumber(&tmpNum1);
	initNumber(&result);
	list *tmp = num1->head, *temp;
	int nodesNum2 = NodesInNumber(num2), flag = 0;
	int j = 0, i = 1;
	int lenFracNum2 = nodesNum2 - num2->pos;
	num1->pos += lenFracNum2;
	num2->pos = nodesNum2;
	if(num1->sign != num2->sign) {
		result->sign = '-';
	}
	result->pos = num1->pos;
	processNumber(&num2);
	while(1) {
		temp = (list *)malloc(sizeof(list));
		temp->next = NULL;
		if(tmp){
			temp->num = tmp->num;
			tmp = tmp->next;
		}
		else{
			if(i <= num1->pos){
				temp->num = 0;
			}
			else if(flag < scale){
				temp->num = 0;
				flag++;
			}
			else{
				processNumber(&result);
				return result;
			}
		}
		if(tmpNum1->head == NULL) {
			temp->pre = NULL;
			tmpNum1->head = tmpNum1->tail = temp;
		}
		else{
			temp->pre = tmpNum1->tail;
			tmpNum1->tail->next = temp;
			tmpNum1->tail = temp;
		}
		(tmpNum1->pos)++;
		j = 0;
		processNumber(&tmpNum1);
		while(abscmp(tmpNum1, num2) >= 0){
			tmpNum1 = subtract(tmpNum1, num2);
			processNumber(&tmpNum1);
			j++;
		}
		temp = (list *)malloc(sizeof(list));
		temp->num = j;
		temp->next = NULL;
		if(result->head == NULL) {
			temp->pre = NULL;
			result->head = result->tail = temp;
		}
		else{
			temp->pre = result->tail;
			result->tail->next = temp;
			result->tail = temp;
		}
		i++;
	}
}
void destroy(Number *num){
	list *temp = (*num)->head;
	while(temp){
		(*num)->head = (*num)->head->next;
		if((*num)->head){
			(*num)->tail = (*num)->head->pre = NULL;
		}
		free(temp);
		temp = (*num)->head;
	}
	free(*num);
}





