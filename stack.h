#include"project.h"
typedef struct node1 {
	Number val;
	struct node1 *next;
}node1;
typedef node1 *opndstack;
typedef struct node2 {
	char ch;
	struct node2 *next;
}node2;
typedef node2 *optrstack;

void initopnd(opndstack *s);
void pushopnd(opndstack *s, Number num);
Number popopnd(opndstack*s);
int isemptyopnd(opndstack *s);
int isfullopnd(opndstack *s);

void initoptr(optrstack *s) ;
void pushoptr(optrstack *s, char c);

char popoptr(optrstack *);

int isemptyoptr(optrstack s);
int isfulloptr(optrstack *s);
